# Weather Report Rails App

## Introduction

This Rails application provides users with weather information based on their given zipcode. Users can input their ZIP code, and the app retrieves and displays the current temperature, along with a seven-day forecast and hourly forecast.

## Features

- **Weather Report**: Get the current temperature and a seven-day forecast based on your ZIP code.
- **Caching**: Weather data is cached for improved performance and reduced API calls.
- **Easy to Use**: Simply input your ZIP code, and the app does the rest.

## Technologies Used

- Ruby on Rails
- HTML/CSS
- React js
- HTTParty gem for API requests
- Bootstrap for styling

## Setup

1. **Install Dependencies**: `bundle install`
2. **Set Environment Variables**: Create a `.env` file and add your API keys and other sensitive information.
3. **Run Migrations**: `rails db:migrate`(Not required at this time)
4. **Start the Server**: `./bin/dev`
5. **Visit the App**: Open your web browser and go to `http://localhost:3000`.

## Configuration

- **API Keys**: You'll need username for services like Geonames. Add these details to your `.env` file.
- **Caching**: Adjust the caching settings in the `config/application.rb` file to meet your requirements.
