import React, { useState } from "react";
import { Link } from "react-router-dom";

const WeatherApp = () => {
  const [zipcode, setZipcode] = useState('');
  const [currentWeather, setCurrentWeather] = useState(null);
  const [sevenDayForecast, setSevenDayForecast] = useState(null);
  const [hourlyForecast, setHourlyForecast] = useState(null);
  const [location, setLocation] = useState(null);
  const [error, setError] = useState(null);
  const [isfromCache, setIsFromCache] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [tz, setTz] = useState('GMT');

  const handleSearch = async () => {
    try {
      if(zipcode.length == 0)
      {
        setError("Please enter Zip code to search")
        return;
      }
      const weatherData = await fetchWeatherData(zipcode);
      if(weatherData == undefined) return;
      setCurrentWeather(weatherData.current);
      setSevenDayForecast(weatherData.daily_forecast);
      setHourlyForecast(weatherData.hourly_forecast);
      setLocation(weatherData.location);
      setIsFromCache(weatherData.from_cache);
      setError(null);
    } catch (error) {
      setError(error);
    }
  };

  const fetchWeatherData = async (zipcode) => {
    setIsLoading(true)
    const weatherResponse = await fetch(`/get_weather_report/${zipcode}`);
    const weatherData = await weatherResponse.json();
    if(weatherResponse.status == 200) {
      setIsLoading(false);
      setTz(weatherData.timezone);
      return {
        location: weatherData.location,
        current: weatherData.current_weather,
        daily_forecast: weatherData.seven_day_forecast,
        hourly_forecast: weatherData.hourly_forecast,
        from_cache: weatherData.from_cache
      };
    }
    else {
      setError(weatherData.error);
      setCurrentWeather(null);
      setSevenDayForecast(null);
      setHourlyForecast(null);
      setLocation(null);
      setIsFromCache(null);
    }
    setIsLoading(false);
  };

  return (
    <div className="container weather-app-wrapper" >
      <div className="row py-3">
        <div className="col-lg-9 mx-auto text-white text-center">
          <h1 className="display-4"> Weather App</h1>
        </div>
      </div>
      {
        error && (
          <div className="d-flex justify-content-center align-items-center">
            <div className="alert alert-danger" role="alert">
              Error fetching weather data: <b>{error}</b>
            </div>
          </div>
        )

      }
      <div className="d-flex justify-content-center">
        <div className="input-group mb-3" style={{ maxWidth: '400px' }}>
          <input
            type="text"
            className="form-control"
            placeholder="Enter zipcode"
            aria-label="Search query"
            aria-describedby="basic-addon2"
            value={zipcode} onChange={(e) => { setZipcode(e.target.value); setError(''); }}
          />
          <div className="input-group-append">
            <button className="btn btn-primary" type="button" onClick={handleSearch}>Search</button>
          </div>
        </div>
      </div>
      {
        isLoading && <div className="d-flex justify-content-center">
          <div className="spinner-border spinner-border-md text-light" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      }
      { 
        !isLoading && currentWeather && <div className="container">
          <div className="d-flex justify-content-center">
            <p className="font-weight-light pl-5 font-italic text-white">From Cache: { isfromCache ?<span class="badge bg-success">true</span> : <span class="badge bg-danger">false</span> }</p>
          </div>
        </div>
      }
      {!isLoading && currentWeather && (
        <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{location}</h5>
                <p className="card-text"><b>Tempreature:</b> {currentWeather.temperature}°C</p>
                <p className="card-text"> <b>High:</b> {currentWeather.high}°C , <b>Low:</b> {currentWeather.low}°C</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      )}
      
      {
        !isLoading && hourlyForecast && (
          <div className="container">
            <div className="row justify-content-center mt-2">
              <div className="col-md-10">
                <h5 class="font-weight-light pl-5 font-italic text-white">Hourly Forecast:</h5>
                <div className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Time({tz})</th>
                        {hourlyForecast.map((hour, index) => (
                          <th key={index}>{hour.time}</th>
                        ))}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Temperature (°C)</td>
                        {hourlyForecast.map((hour, index) => (
                          <td key={index}>{hour.temperature}</td>
                        ))}
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      )}
      
      {
        !isLoading && sevenDayForecast && (
          <div className="container">
            <div className="row justify-content-center mt-2">
              <div className="col-md-10">
                <div className="table-responsive">
                  <h5 class="font-weight-light pl-5 font-italic text-white">7 day Forecast:</h5>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th><p>Day & Date ({tz})</p></th>
                        {sevenDayForecast.map((hour, index) => (
                          <th key={index}> <p>{ index == 0 ? 'Today' : hour.day}</p> <p>{hour.date}</p></th>
                        ))}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Temperature (°C)</td>
                        {sevenDayForecast.map((hour, index) => (
                          <td key={index}><b>H:</b> {hour.high}, <b>L:</b> {hour.low}</td>
                        ))}
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      )}


    </div>
  );
};

export default WeatherApp;