import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import WeatherApp from "../components/WeatherApp";

export default (
  <Router>
    <Routes>
      <Route path="/" element={<WeatherApp />} />
    </Routes>
  </Router>
);