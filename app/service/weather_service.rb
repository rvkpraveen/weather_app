# app/services/weather_service.rb
require 'httparty'

class WeatherService
  BASE_URL = 'https://api.open-meteo.com/v1'

  def self.get_current_weather(lat, lon, timezone='GMT')
    url = "#{BASE_URL}/forecast?latitude=#{lat}&longitude=#{lon}&current=temperature_2m&daily=temperature_2m_max,temperature_2m_min&forecast_days=1&timezone=#{timezone}"
    response = HTTParty.get(url)

    if response.success?
      result = JSON.parse(response.body)
      temperature = result['current']['temperature_2m']

      # Since this API doesnt return high and low  am hardcoding it here
      low = result['daily']['temperature_2m_min'][0]
      high = result['daily']['temperature_2m_max'][0]

      return { temperature: temperature, low: low, high: high }
    else
      return nil
    end
  end

  def self.get_hourly_forecast(lat, lon, timezone='GMT')
    url = "#{BASE_URL}/forecast?latitude=#{lat}&longitude=#{lon}&current=temperature_2m&hourly=temperature_2m&forecast_hours=10&timezone=#{timezone}"

    response = HTTParty.get(url)

    if response.success?
      result = JSON.parse(response.body)
      forecast = get_hourly_forecast_data(result["hourly"])
      return forecast
    else
      return nil
    end
  end

  def self.get_seven_day_forecast(lat, lon, timezone='GMT')
    url = "#{BASE_URL}/forecast?latitude=#{lat}&longitude=#{lon}&daily=temperature_2m_max,temperature_2m_min&timezone=#{timezone}"
    response = HTTParty.get(url)

    if response.success?
      result = JSON.parse(response.body)
      forecast = get_forecast_data(result["daily"])
      return forecast
    else
      return nil
    end
  end

  private

  def self.get_forecast_data(response)
    length = response["time"].length
    response_data = []

    (0...length).each do |i|
      date_time = response["time"][i].to_datetime
      response_data << {day: date_time.strftime('%A'), date: date_time.strftime('%d-%m-%Y'), high: response["temperature_2m_max"][i], low: response["temperature_2m_min"][i]}
    end
    response_data
  end

  def self.get_hourly_forecast_data(response)
    length = response["time"].length
    response_data = []

    (0...length).each do |i|
      date_time = response["time"][i].to_datetime
      response_data << {time:  date_time.strftime('%I %p'), temperature:  response["temperature_2m"][i]}
    end
    response_data
  end
end
