require 'httparty'
require 'geocoder'

class LocationService
  BASE_URL = 'http://api.geonames.org/'
  API_USER_NAME = ENV['API_USER_NAME']

  def self.convert_zip_to_coordinates(zip_code)
    url = "#{BASE_URL}postalCodeSearchJSON?postalcode=#{zip_code}&maxRows=1&username=#{API_USER_NAME}"
    response = HTTParty.get(url)

    if response.success?
      result = JSON.parse(response.body)
      if result['postalCodes'].any?
        lat = result['postalCodes'][0]['lat']
        lng = result['postalCodes'][0]['lng']
        location = format_place_name(result['postalCodes'][0])
        return [lat, lng, location ]
      else
        # ZIP code not found
        return [nil, nil, nil]
      end
    else
      # Handle HTTP error
      return [nil, nil, nil]
    end
  end

  def self.lat_long_to_timezone(lat, long)
    url = "#{BASE_URL}timezoneJSON?lat=#{lat}&lng=#{long}&username=#{API_USER_NAME}"
    response = HTTParty.get(url)

    if response.success?
      result = JSON.parse(response.body)
      result['timezoneId'] || 'GMT'
    else
      'GMT'
    end
  rescue
    'GMT'
  end


  private

  def self.format_place_name(data)
    return if data.blank?
    place_name = ''
    place_name = data['placeName']
    place_name = place_name + ", #{data['adminName2']}" if data['adminName2'].present?
    place_name = place_name + ", #{data['adminName1']}" if data['adminName1'].present?
    place_name = place_name + ", #{data['countryCode']}" if data['countryCode'].present?
  end
end
