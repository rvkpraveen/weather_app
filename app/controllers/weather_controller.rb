class WeatherController < ApplicationController
  CACHE_EXPIRATION_TIME = 30.minutes
  VERSION = 1
  
  def index
  end

  def get_weather_report
    @zip_code = params.require(:zip_code)
    render json: { error: "Please pass valid zip code" }, status: :unprocessable_entity and return if @zip_code.blank?
    
    lat, lon, location = LocationService.convert_zip_to_coordinates(@zip_code)
    render json: { error: "Given Location is not valid" }, status: :unprocessable_entity and return if(lat.blank? || lon.blank?)

    render json: get_weather_report_data(lat, lon).merge({location: location})
  rescue => e
    puts e
    render json: { error: 'Something went wrong. Please try again after sometime' }, status: :unprocessable_entity
  end

  private

  def get_weather_report_data(lat, lon)
    # VERSION needs to be updated when the data contract changes to avoid conflict
    cache_key = "v_#{VERSION}_data_#{@zip_code}"
    cached_data = Rails.cache.read(cache_key)

    if cached_data
      cached_data['from_cache'] = true
      return cached_data
    else
      timezone = LocationService.lat_long_to_timezone(lat, lon)
      data = {
        current_weather: WeatherService.get_current_weather(lat, lon, timezone),
        seven_day_forecast: WeatherService.get_seven_day_forecast(lat, lon, timezone),
        hourly_forecast:  WeatherService.get_hourly_forecast(lat, lon, timezone),
        timezone: timezone
      }
      Rails.cache.write(cache_key, data, expires_in: CACHE_EXPIRATION_TIME )
      data['from_cache'] = false
      data
    end
  end
end
