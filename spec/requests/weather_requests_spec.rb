require 'rails_helper'

RSpec.describe WeatherController, type: :controller do
  describe 'GET #get_weather_report' do
    let(:valid_zip_code) { '605007' }
    let(:invalid_zip_code) { 'invalid' }
    let(:lat) { '12.345' }
    let(:lon) { '67.890' }
    let(:location) { 'Thavalakuppam, Puducherry, Puducherry, IN' }
    let(:weather_data) do
      {
        temperature: 25,
        low: 20,
        high: 30
      }
    end

    context 'with valid zip code' do
      before do
        allow(LocationService).to receive(:convert_zip_to_coordinates).with(valid_zip_code).and_return([lat, lon, location])
        allow(controller).to receive(:get_weather_report_data).with(lat, lon).and_return(weather_data)
        get :get_weather_report, params: { zip_code: valid_zip_code }
      end

      it 'returns HTTP success' do
        expect(response).to have_http_status(:success)
      end

      it 'returns the weather report data' do
        expect(response.body).to eq(weather_data.merge({ location: location }).to_json)
      end
    end

    context 'with invalid zip code' do
      before do
        allow(LocationService).to receive(:convert_zip_to_coordinates).with(invalid_zip_code).and_return([nil, nil, nil])
        get :get_weather_report, params: { zip_code: invalid_zip_code }
      end

      it 'returns HTTP unprocessable entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns an error message' do
        parsed_response = JSON.parse(response.body)
        expect(parsed_response["error"]).to include('Given Location is not valid')
      end
    end
  end
end
