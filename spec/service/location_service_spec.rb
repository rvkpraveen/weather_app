require 'rails_helper'
require 'webmock/rspec'

RSpec.describe LocationService do
  describe '.convert_zip_to_coordinates' do
    let(:zip_code) { '605007' }
    let(:lat) { '11.852' }
    let(:lng) { '79.796' }
    let(:location) { 'Thavalakuppam, Puducherry, Puducherry, IN' }

    context 'with valid zip code' do
      before do
        stub_request(:get, "http://api.geonames.org/postalCodeSearchJSON?maxRows=1&postalcode=#{zip_code}&username=#{ENV['API_USER_NAME']}")
          .to_return(status: 200, body: {
            'postalCodes' => [{
              'lat' => lat,
              'lng' => lng,
              'placeName' => 'Thavalakuppam',
              'adminName2' => 'Puducherry',
              'adminName1' => 'Puducherry',
              'countryCode' => 'IN'
            }]
          }.to_json)
      end

      it 'returns coordinates and location' do
        expect(LocationService.convert_zip_to_coordinates(zip_code)).to eq([lat, lng, location])
      end
    end

    context 'with invalid zip code' do
      before do
        stub_request(:get, "http://api.geonames.org/postalCodeSearchJSON?maxRows=1&postalcode=invalid_zip_code&username=#{ENV['API_USER_NAME']}")
          .to_return(status: 200, body: { 'postalCodes' => [] }.to_json)
      end

      it 'returns nil for coordinates and location' do
        expect(LocationService.convert_zip_to_coordinates('invalid_zip_code')).to eq([nil, nil, nil])
      end
    end

    context 'when API request fails' do
      before do
        stub_request(:get, "http://api.geonames.org/postalCodeSearchJSON?maxRows=1&postalcode=#{zip_code}&username=#{ENV['API_USER_NAME']}")
          .to_return(status: 500)
      end

      it 'returns nil for coordinates and location' do
        expect(LocationService.convert_zip_to_coordinates(zip_code)).to eq([nil, nil, nil])
      end
    end
  end

  describe '.format_place_name' do
    let(:data) do
      {
        'placeName' => 'Puducherry',
        'adminName2' => 'India',
        'adminName1' => 'Puducherry',
        'countryCode' => 'IN'
      }
    end

    it 'formats the place name correctly' do
      formatted_name = LocationService.send(:format_place_name, data)
      expect(formatted_name).to eq('Puducherry, India, Puducherry, IN')
    end

    context 'when some fields are missing' do
      let(:data) do
        {
          'placeName' => 'Puducherry',
          'adminName1' => 'Puducherry',
          'countryCode' => 'IN'
        }
      end

      it 'formats the place name correctly with missing fields' do
        formatted_name = LocationService.send(:format_place_name, data)
        expect(formatted_name).to eq('Puducherry, Puducherry, IN')
      end
    end

    context 'when all fields are missing' do
      let(:data) { {} }

      it 'returns an empty string' do
        formatted_name = LocationService.send(:format_place_name, data)
        expect(formatted_name).to eq(nil)
      end
    end
  end

  describe '.lat_long_to_timezone' do
    let(:lat) { 40.7128 }
    let(:long) { -74.0060 }

    context 'when API request is successful' do
      before do
        stub_request(:get,"#{LocationService::BASE_URL}timezoneJSON?lat=#{lat}&lng=#{long}&username=#{LocationService::API_USER_NAME}").
          to_return(status: 200, body: { 'timezoneId' => 'Asia/Kolkatta' }.to_json)
      end

      it 'returns the timezone' do
        expect(LocationService.lat_long_to_timezone(lat, long)).to eq('Asia/Kolkatta')
      end
    end

    context 'when API request fails' do
      before do
        stub_request(:get, /#{LocationService::BASE_URL}.*/).
          to_return(status: 500)
      end

      it 'returns GMT' do
        expect(LocationService.lat_long_to_timezone(lat, long)).to eq('GMT')
      end
    end

    context 'when API request throws an error' do
      before do
        allow(HTTParty).to receive(:get).and_raise(StandardError)
      end
      
      it 'returns GMT' do
        expect(LocationService.lat_long_to_timezone(lat, long)).to eq('GMT')
      end
    end
  end

end
