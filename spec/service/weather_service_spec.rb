require 'rails_helper'

RSpec.describe WeatherService do
  describe '.get_current_weather' do
    context 'when the API request is successful' do
      let(:lat) { '11.852' }
      let(:lon) { '79.796' }
      let(:temperature) { 25 }
      let(:low) { 20 }
      let(:high) { 30 }
      let(:response_body) do
        {
          'current' => { 'temperature_2m' => temperature },
          'daily' => {
            'temperature_2m_min' => [low],
            'temperature_2m_max' => [high]
          }
        }.to_json
      end

      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: true, body: response_body))
      end

      it 'returns the current weather data' do
        expect(WeatherService.get_current_weather(lat, lon)).to eq({ temperature: temperature, low: low, high: high })
      end
    end

    context 'when the API request fails' do
      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: false))
      end

      it 'returns nil' do
        expect(WeatherService.get_current_weather('11.852', '79.796')).to be_nil
      end
    end
  end

  describe '.get_hourly_forecast' do
    let(:lat) { '11.852' }
    let(:lon) { '79.796' }
    let(:response_body) do
      {
        'hourly' => {
          'time' => ['2024-05-01T12:00:00', '2024-05-01T13:00:00'],
          'temperature_2m' => [25, 26]
        }
      }.to_json
    end

    context 'when the API request is successful' do
      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: true, body: response_body))
      end

      it 'returns the hourly forecast data' do
        expect(WeatherService.get_hourly_forecast(lat, lon)).to eq([
          { time: '12 PM', temperature: 25 },
          { time: '01 PM', temperature: 26 }
        ])
      end
    end

    context 'when the API request fails' do
      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: false))
      end

      it 'returns nil' do
        expect(WeatherService.get_hourly_forecast(lat, lon)).to be_nil
      end
    end
  end

  describe '.get_seven_day_forecast' do
    let(:lat) { '11.852' }
    let(:lon) { '79.796' }
    let(:response_body) do
      {
        'daily' => {
          'time' => ['2024-05-01', '2024-05-02'],
          'temperature_2m_max' => [30, 32],
          'temperature_2m_min' => [20, 22]
        }
      }.to_json
    end

    context 'when the API request is successful' do
      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: true, body: response_body))
      end

      it 'returns the seven-day forecast data' do
        expect(WeatherService.get_seven_day_forecast(lat, lon)).to eq([
          { day: 'Wednesday', date: '01-05-2024', high: 30, low: 20 },
          { day: 'Thursday', date: '02-05-2024', high: 32, low: 22 }
        ])
      end
    end

    context 'when the API request fails' do
      before do
        allow(HTTParty).to receive(:get).and_return(double(success?: false))
      end

      it 'returns nil' do
        expect(WeatherService.get_seven_day_forecast(lat, lon)).to be_nil
      end
    end
  end
end
