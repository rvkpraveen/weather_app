Rails.application.routes.draw do
  root "weather#index"
  get "/get_weather_report/:zip_code", to: "weather#get_weather_report"
end
